package procedure.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import procedure.model.FilePropertiesModel;
import procedure.repository.FileStorageResponsitory;

@Service
public class FileDaoService {
    @Autowired
    private FileStorageResponsitory fileStorageResponsitory;

    public void saveFiletoSQL(FilePropertiesModel filePropertiesModel) {
        fileStorageResponsitory.save(filePropertiesModel);
    }
}
