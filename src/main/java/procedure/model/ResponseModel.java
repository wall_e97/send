package procedure.model;

import javax.persistence.Entity;

public class ResponseModel {
	private String error;
	private String message_error;
	private Object data;

	public ResponseModel(String error, String message_error, Object data) {
		super();
		this.error = error;
		this.message_error = message_error;
		this.data = data;
	}

	public ResponseModel() {
		super();
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage_error() {
		return message_error;
	}

	public void setMessage_error(String message_error) {
		this.message_error = message_error;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
