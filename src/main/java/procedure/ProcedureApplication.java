package procedure;

import procedure.model.FileStorageModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
		FileStorageModel.class
})
public class ProcedureApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProcedureApplication.class, args);
	}
}
